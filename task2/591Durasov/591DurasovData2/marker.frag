/**
Простой шейдер для отрисовки маркера источника света
*/

#version 330

uniform vec4 color;

out vec4 fragColor;

void main()
{
	fragColor = vec4(0, 0., 1., 0.5);
}
