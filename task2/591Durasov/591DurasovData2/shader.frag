#version 330

uniform sampler2D grassTex;
uniform sampler2D gravTex;
uniform sampler2D rockTex;
uniform sampler2D sandTex;
uniform sampler2D maskTex;

struct LightInfo
{
	vec3 pos;
	vec3 La;
	vec3 Ld;
	vec3 Ls;
};

uniform LightInfo light;

in vec3 normalCamSpace;
in vec4 posCamSpace;
in vec2 texCoord;
out vec4 fragColor;

const vec3 KsSand = vec3(0, 0, 0);
const vec3 KsGrass = vec3(0.0, 0.0, 0.0);
const vec3 KsRock = vec3(1., 1.0, 1.0);
const vec3 KsGrav = vec3(0.0, 0.0, 0.0);
const float shininess = 10.0;

void main()
{
	vec4 maskColor = texture(maskTex, texCoord).rgba;

	vec3 grassColor = texture(grassTex, texCoord).rgb * maskColor.r;
	vec3 gravColor = texture(gravTex, texCoord).rgb * maskColor.g;
	vec3 rockColor = texture(rockTex, texCoord).rgb * maskColor.b;
	vec3 sandColor = texture(sandTex, texCoord).rgb * maskColor.a;

	vec3 normal = normalize(normalCamSpace);
	vec3 viewDirection = normalize(-posCamSpace.xyz);

	vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz);
	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0);
	vec3 color = (grassColor + gravColor + rockColor + sandColor) * (light.La + light.Ld * NdotL);

	vec3 Ks = KsGrass * maskColor.r + KsGrav * maskColor.g + KsRock * maskColor.b + KsSand * maskColor.a;

	if (NdotL > 0.0)
	{
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);
		float blinnTerm = max(dot(normal, halfVector), 0.0);
		blinnTerm = pow(blinnTerm, shininess);
		color += light.Ls * Ks * blinnTerm;
	}

	fragColor = vec4(color, 1.0);
}
