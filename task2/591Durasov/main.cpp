#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <fstream>

glm::vec3 norm(glm::vec3 first, glm::vec3 second, glm::vec3 third){
    return glm::cross(first-second, first-third);

std::vector<std::vector<float>> ReadTerrain() {
    std::vector<std::vector<float>> terrain(512, std::vector<float>(512, 0));

    std::string file_path = "./591DurasovData2/terrain.txt";
    std::fstream fs;
    fs.open(file_path, std::fstream::in);
    for (int i = 0; i < 512; i++) {
        for (int j = 0; j < 512; j++) {
            float value;
            fs >> value;
            terrain[i][j] = value + 20 * pow(value, 3);

        }
    }
    fs.close();

    return terrain;
}

MeshPtr makeSurface(unsigned int N = 100, int size = 5, int h = 10, int tiles=5, float scale=1.)
{

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texture;

    int sz = 511;
    std::vector<std::vector<float>> heights = ReadTerrain();
    float texStep = 1. * tiles / 511;

    for (int i = 0; i < sz; i++) {
        for (int j = 0; j < sz; j++) {

            // first triangle
            vertices.push_back(glm::vec3(i*scale, j*scale, heights[i][j]));
            vertices.push_back(glm::vec3((i+1)*scale, j*scale, heights[i+1][j]));
            vertices.push_back(glm::vec3(i*scale, (j+1)*scale, heights[i][j+1]));

            glm::vec3 norm1 = norm(
                    glm::vec3(i*scale, j*scale, heights[i][j]),
                    glm::vec3((i+1)*scale, j*scale, heights[i+1][j]),
                    glm::vec3(i*scale, (j+1)*scale, heights[i][j+1])
            );

            normals.push_back(norm1);
            normals.push_back(norm1);
            normals.push_back(norm1);

            texture.push_back(glm::vec2(i*texStep, j*texStep));
            texture.push_back(glm::vec2((i+1)*texStep, j*texStep));
            texture.push_back(glm::vec2(i*texStep, (j+1)*texStep));


            // second triangle
            vertices.push_back(glm::vec3((i+1)*scale, j*scale, heights[i+1][j]));
            vertices.push_back(glm::vec3(i*scale, (j+1)*scale, heights[i][j+1]));
            vertices.push_back(glm::vec3((i+1)*scale, (j+1)*scale, heights[i+1][j+1]));

            glm::vec3 norm2 = norm(
                    glm::vec3((i+1)*scale, j*scale, heights[i+1][j]),
                    glm::vec3(i*scale, (j+1)*scale, heights[i][j+1]),
                    glm::vec3((i+1)*scale, (j+1)*scale, heights[i+1][j+1])
            );

            normals.push_back(-1.0f * norm2);
            normals.push_back(-1.0f * norm2);
            normals.push_back(-1.0f * norm2);

            texture.push_back(glm::vec2((i+1)*texStep, j*texStep));
            texture.push_back(glm::vec2(i*texStep, (j+1)*texStep));
            texture.push_back(glm::vec2((i+1)*texStep, (j+1)*texStep));
        }
    }

    // set mesh
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texture.size() * sizeof(float) * 2, texture.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


class SampleApplication : public Application
{
public:

    MeshPtr _surface;
    MeshPtr _marker;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    float _lr = 30.0f;
    float _phi = 7.5f;
    float _theta = 7.5f;

    LightInfo _light;

    TexturePtr _grassTex;
    TexturePtr _gravTex;
    TexturePtr _rockTex;
    TexturePtr _sandTex;
    TexturePtr _maskTex;

    GLuint _samplerGrass;
    GLuint _samplerGrav;
    GLuint _samplerRock;
    GLuint _samplerSand;
    GLuint _samplerMask;

    void makeScene() override
    {
        Application::makeScene();

        _surface = makeSurface(100, 5, 10, 10, 0.3);
        _surface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));


        _marker = makeSphere(0.9f);

        _cameraMover = std::make_shared<FreeCameraMover>();

        _shader = std::make_shared<ShaderProgram>("591DurasovData2/shader.vert", "591DurasovData2/shader.frag");
        _markerShader = std::make_shared<ShaderProgram>("591DurasovData2/marker.vert", "591DurasovData2/marker.frag");

        float sc1 = 0.1;
        float sc2 = 0.2;
        float sc3 = 0.5;

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(sc1, sc1, sc1);
        _light.diffuse = glm::vec3(sc2, sc2, sc2);
        _light.specular = glm::vec3(sc3, sc3, sc3);

        _grassTex = loadTexture("591DurasovData2/grass.jpg");
        _gravTex = loadTexture("591DurasovData2/grav.jpg");
        _rockTex = loadTexture("591DurasovData2/rock.jpg");
        _sandTex = loadTexture("591DurasovData2/sand.jpg");
        _maskTex = loadTexture("591DurasovData2/mask.png");


        glGenSamplers(1, &_samplerGrass);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerGrass, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerGrav);
        glSamplerParameteri(_samplerGrav, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerGrav, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerGrav, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerGrav, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerRock);
        glSamplerParameteri(_samplerRock, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerRock, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerRock, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerRock, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerSand);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerSand, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenSamplers(1, &_samplerMask);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_samplerMask, GL_TEXTURE_WRAP_T, GL_REPEAT);


    }

    void draw() override
    {

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();


        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);


        glBindSampler(0, _samplerGrass);
        glActiveTexture(GL_TEXTURE0);
        _grassTex->bind();
        _shader->setIntUniform("grassTex", 0);

        glBindSampler(1, _samplerRock);
        glActiveTexture(GL_TEXTURE1);
        _rockTex->bind();
        _shader->setIntUniform("gravTex", 1);

        glBindSampler(2, _samplerSand);
        glActiveTexture(GL_TEXTURE2);
        _sandTex->bind();
        _shader->setIntUniform("rockTex", 2);

        glBindSampler(3, _samplerMask);
        glActiveTexture(GL_TEXTURE3);
        _maskTex->bind();
        _shader->setIntUniform("maskTex", 3);

        glBindSampler(4, _samplerGrav);
        glActiveTexture(GL_TEXTURE4);
        _gravTex->bind();
        _shader->setIntUniform("sandTex", 4);


        {
            _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
            _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _surface->modelMatrix()))));
            _surface->draw();
        }

        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();


        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}